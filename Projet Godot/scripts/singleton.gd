extends Node

var Index_base4 = 0
var Index_base10 = 0

var code1
var code2
var code3
var code4

var Mot_De_Passe = "lama"

var Path = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS, true)
var android_permission = OS.request_permissions()

var config_app = ConfigFile.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	android_permission = true
	_chargement_mot_de_passe()
	pass # Replace with function body.


func _sauvegarde_mot_de_passe():
	config_app.set_value("App", "mdp", Mot_De_Passe)
	config_app.save("user://donnees_app.cfg")


func _chargement_mot_de_passe():
	var err = config_app.load("user://donnees_app.cfg")
	if err != OK:
		print("erreur de chargement de la langue")
		return
	for App in config_app.get_sections():
		var chargement_mdp = config_app.get_value("App", "mdp")
		Mot_De_Passe = chargement_mdp



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass


#func _sauvegarde_zip(): # https://docs.godotengine.org/fr/4.x/classes/class_zippacker.html
	#var writer := ZIPPacker.new()
	#var err := writer.open("user://archive.zip")
	#if err != OK:
		#return err
	#writer.start_file("user://illustration_1.jpg")
	#writer.close()
	##writer.start_file("hello.txt")
	##writer.write_file("Hello World".to_utf8_buffer())
	##writer.close_file()
	##writer.close()
	##for i in range (1,5):
		##writer.start_file("user://illustration_" + str(i) + ".jpg")
		##pass
	#return OK


func _conversion_base4_vers_base10(nombre):
	var chiffre4 = nombre / 1000
	var chiffre3 = nombre / 100 - (chiffre4 * 10)
	var chiffre2 = nombre / 10 - (chiffre4 * 100) - (chiffre3 * 10)
	var chiffre1 = nombre - chiffre4 * 1000 - chiffre3 * 100 - chiffre2 * 10
	#print("résultat : " + str(chiffre4) + " " + str(chiffre3) + " " + str(chiffre2) + " " + str(chiffre1))
	var resultat = chiffre4 * pow(4,3) + chiffre3 * pow(4,2) + chiffre2 * pow(4,1) + chiffre1 * pow(4,0)
	#print(resultat)
	return resultat


func _conversion_base10_vers_base4(nombre):
	if nombre == 0 : return "0"
	var resultat = ""
	while nombre > 0 :
		var reste = int(nombre) % 4
		resultat = str(reste) + resultat
		#nombre = int(nombre) / 4
		nombre = int(nombre / 4)
	return resultat


