extends Control

@export var index_audio = 1

var lecture_temps
var lecture_minutes
var lecture_secondes
var audio_longueur
var minutes
var secondes
var position_lecteur

# Called when the node enters the scene tree for the first time.
func _ready():
	_chargement_musique(index_audio)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if $AudioStreamPlayer.playing == true :
		$HSlider.value = $AudioStreamPlayer.get_playback_position()
	_temps_lecture()




func _chargement_musique(index):
	var mon_fichier = FileAccess.open("user://audio_" + str(index) + ".mp3", FileAccess.READ)
	if mon_fichier == null :
		print("le fichier audio n'existe pas")
		$AudioStreamPlayer.stream = load("res://assets/audio/audio_0.mp3")
		_masquer_lecteur()
		return
	else :
		_afficher_lecteur()
		var mon_audio = AudioStreamMP3.new()
		mon_audio.data = mon_fichier.get_buffer(mon_fichier.get_length())
		$AudioStreamPlayer.stream = mon_audio
		_temps_musique()
	pass

func _masquer_lecteur():
	$TextureButton_play.hide()
	$Label_temps_lecture.hide()
	$Label_temps_total.hide()
	$HSlider.hide()
	$Panel.hide()
	$LogoAudioNon.show()


func _afficher_lecteur():
	$Panel.show()
	$TextureButton_play.show()
	$Label_temps_lecture.show()
	$Label_temps_total.show()
	$HSlider.show()
	$LogoAudioNon.hide()
	pass




func _temps_lecture():
	lecture_temps = $AudioStreamPlayer.get_playback_position()
	lecture_minutes = int(lecture_temps / 60)
	lecture_secondes = int(lecture_temps - lecture_minutes * 60)
	$Label_temps_lecture.text = str(lecture_minutes) + "." + str(lecture_secondes)

func _temps_musique():
	audio_longueur = $AudioStreamPlayer.stream.get_length()
	minutes = int(audio_longueur / 60)
	secondes = int(audio_longueur - minutes * 60)
	print(str(minutes) + " min " + str(secondes) + " sec")
	$Label_temps_total.text = str(minutes) + "." + str(secondes)
	$HSlider.max_value = audio_longueur
	print(audio_longueur)


func _on_texture_button_play_pressed():
	if $AudioStreamPlayer.playing == false && $AudioStreamPlayer.stream_paused == false :
		$AudioStreamPlayer.play()
		$TextureButton_play.button_pressed = true
	elif $AudioStreamPlayer.playing == false && $AudioStreamPlayer.stream_paused == true :
		$AudioStreamPlayer.stream_paused = false
		$TextureButton_play.button_pressed = true
	else :
		$AudioStreamPlayer.stream_paused = true
		$TextureButton_play.button_pressed = false
	pass # Replace with function body.




func _on_h_slider_drag_started():
	if $AudioStreamPlayer.playing == true :
		$AudioStreamPlayer.stream_paused = true



func _on_h_slider_drag_ended(_value_changed):
	position_lecteur = $HSlider.value
	print(position)
	$AudioStreamPlayer.play(position_lecteur)
	$TextureButton_play.button_pressed = true






