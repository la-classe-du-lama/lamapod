extends ColorRect

@export var index_illustration = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	_chargement_illustration(index_illustration)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass

func _chargement_illustration(index):
	#var adresse = "res://assets/illustrations/illustration_" + str(index) + ".png"
	var adresse = "user://illustration_" + str(index) + ".jpg"
	var mon_image = Image.new()
	var err = FileAccess.file_exists(adresse)
	if err == true :
		mon_image.load(adresse)
		var ma_texture = ImageTexture.create_from_image(mon_image)
		$TextureRect.texture = ma_texture
	else :
		print("L'image n'existe pas.")
		$TextureRect.texture = load("res://assets/illustrations/illustration_0.png")
	pass
	#if mon_image.is_empty() == true :
		#print("L'image n'existe pas.")
		#$TextureRect.texture = load("res://assets/illustrations/illustration_0.png")
		#return
	#else :
		#var ma_texture = ImageTexture.create_from_image(mon_image)
		#$TextureRect.texture = ma_texture
	pass
