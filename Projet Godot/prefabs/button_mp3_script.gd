extends Button

@export var valeur = 1
var valeur_base4
var nb1
var nb2
var nb3
var nb4

# Called when the node enters the scene tree for the first time.
func _ready():
	text = str(valeur)
	valeur_base4 = _conversion_base_4(valeur)
	print(str(valeur_base4))
	_decomposer_nombre(valeur_base4)
	print(str(nb1), str(nb2), str(nb3), str(nb4))
	_code_couleur($ColorRect4, nb1)
	_code_couleur($ColorRect3, nb2)
	_code_couleur($ColorRect2, nb3)
	_code_couleur($ColorRect1, nb4)
	pass # Replace with function body.



func _conversion_base_4(n):
	if n < 4 :
		return str(n)
	else :
		return _conversion_base_4(n / 4) + str(n % 4)


func _decomposer_nombre(n):
	var nombre = str(n)
	nb1 = int(nombre[0])
	if nombre.length() > 1 :
		nb2 = int(nombre[1])
	if nombre.length() > 2:
		nb3 = int(nombre[2])
	if nombre.length() > 3 :
		nb4 = int(nombre[3])
	pass

func _code_couleur(cible,monnombre):
	if str(monnombre) == "0" : cible.color = "black"
	if str(monnombre) == "1" : cible.color = "red"
	if str(monnombre) == "2" : cible.color = "blue"
	if str(monnombre) == "3" : cible.color = "green"
	pass
