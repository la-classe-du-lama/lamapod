extends TextEdit


func _chargement_titre(nombre):
	var err = FileAccess.file_exists("user://titre_" + str(nombre) + ".txt")
	if err == true :
		var mon_fichier = FileAccess.open("user://titre_" + str(nombre) + ".txt" , FileAccess.READ)
		var mon_contenu = mon_fichier.get_as_text()
		#return mon_contenu
		self.text = mon_contenu
	else :
		self.text = ""
	pass


func _sauvegarde_titre(nombre):
	if self.editable == true :
		var mon_titre = self.get_text()
		if mon_titre != "" :
			var mon_fichier = FileAccess.open("user://titre_" + str(nombre) + ".txt", FileAccess.WRITE)
			mon_fichier.store_string(mon_titre)
		else :
			var err = FileAccess.file_exists("user://titre_" + str(nombre) + ".txt")
			if err == true :
				DirAccess.remove_absolute("user://titre_" + str(nombre) + ".txt")

