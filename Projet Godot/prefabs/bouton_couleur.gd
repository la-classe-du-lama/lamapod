extends TextureButton

@export var valeur = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _changement_couleur():
	var adresse = "res://assets/interface/cercle_" + str(valeur) + ".png"
	self.texture_normal = load(adresse)
	$Label.text = str(valeur)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass


func _on_pressed():
	if valeur == 3 : valeur = 0
	else : valeur += 1
	print(valeur)
	_changement_couleur()
	$Label.text = str(valeur)
	pass # Replace with function body.
