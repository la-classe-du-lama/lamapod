extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass


func _on_button_demarrer_pressed():
	get_tree().change_scene_to_file("res://scenes/code.tscn")
	pass # Replace with function body.


func _on_button_configuration_pressed():
	get_tree().change_scene_to_file("res://scenes/mot_de_passe_v2.tscn")


func _on_button_close_pressed():
	$Panel.hide()
	pass # Replace with function body.


func _on_texture_button_pressed():
	$Panel.show()
	pass # Replace with function body.


func _on_button_pressed():
	get_tree().quit()
	pass # Replace with function body.
