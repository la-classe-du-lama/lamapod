extends Control

var index = 1


func _ready():
	_changer_repertoire_defaut()
	_chargement_donnees()


func _changer_repertoire_defaut():
	$FileDialog_image.current_path = Singleton.Path
	$FileDialog_audio.current_path = Singleton.Path
	$FileDialog_export.current_path = Singleton.Path
	$FileDialog_import.current_path = Singleton.Path
	pass



func _chargement_donnees():
	$Label.text = "fiche n° " + str(index) + " / 255"
	$TextEdit._chargement_titre(index)
	$ColorRect._chargement_illustration(index)
	$Control_audio._chargement_musique(index)
	_affichage_code_couleur(Singleton._conversion_base10_vers_base4(index))
	pass








# ---------------- CODE COULEUR

func _affichage_code_couleur(nombre):
	var str_nombre = str(nombre)
	if str_nombre.length() < 2 : str_nombre = "000" + str_nombre
	if str_nombre.length() < 3 : str_nombre = "00" + str_nombre
	if str_nombre.length() < 4 : str_nombre = "0" + str_nombre
	$gp_boutons/TextureButton_4.valeur = str_nombre[3]
	$gp_boutons/TextureButton_4._changement_couleur()
	$gp_boutons/TextureButton_3.valeur = str_nombre[2]
	$gp_boutons/TextureButton_3._changement_couleur()
	$gp_boutons/TextureButton_2.valeur = str_nombre[1]
	$gp_boutons/TextureButton_2._changement_couleur()
	$gp_boutons/TextureButton_1.valeur = str_nombre[0]
	$gp_boutons/TextureButton_1._changement_couleur()
	pass



func _on_texture_button_retour_pressed():
	get_tree().change_scene_to_file("res://scenes/menu.tscn")


func _on_texture_button_moins_pressed():
	if index > 1 : index -= 1
	_chargement_donnees()
	$Control_audio/TextureButton_play.button_pressed = false
	$Control_audio/HSlider.value = 0
	pass # Replace with function body.


func _on_texture_button_plus_pressed():
	if index < 255 : index += 1
	_chargement_donnees()
	$Control_audio/TextureButton_play.button_pressed = false
	$Control_audio/HSlider.value = 0


func _on_h_slider_value_changed(value):
	index = $HSlider.value
	_chargement_donnees()


func _on_button_load_image_pressed():
	$FileDialog_image.show()




func _on_file_dialog_image_file_selected(path):
	Singleton.Path = path
	print(Singleton.Path)
	_changer_repertoire_defaut()
	var image_chargement = Image.new()
	image_chargement.load(path)
	image_chargement.save_jpg("user://illustration_" + str(index) + ".jpg")
	_chargement_donnees()




#------------------- AUDIO

func _on_button_load_audio_pressed():
	_changer_repertoire_defaut()
	$FileDialog_audio.show()


func _on_file_dialog_audio_file_selected(path):
	Singleton.Path = path
	_changer_repertoire_defaut()
	var destination = DirAccess.open("user://")
	destination.copy(path, "user://audio_"+str(index)+".mp3")
	_chargement_donnees()



func _on_button_effacer_pressed():
	$Button_effacer/ConfirmationDialog_effacer_fiche.show()

func _on_confirmation_dialog_effacer_fiche_confirmed():
	var fichier_image = FileAccess.file_exists("user://illustration_" + str(index) + ".jpg")
	var fichier_audio = FileAccess.file_exists("user://audio_" + str(index) + ".mp3")
	var fichier_titre = FileAccess.file_exists("user://titre_" + str(index) + ".txt")
	if fichier_image == true :
		DirAccess.remove_absolute("user://illustration_" + str(index) + ".jpg")
	if fichier_audio == true :
		DirAccess.remove_absolute("user://audio_" + str(index) + ".mp3")
	if fichier_titre == true :
		DirAccess.remove_absolute("user://titre_" + str(index) + ".txt")
	_chargement_donnees()


func _on_button_effacer_tout_pressed():
	$Button_effacer_tout/ConfirmationDialog_effacer_tout.show()


func _on_confirmation_dialog_effacer_tout_confirmed():
	for n in range (1,256):
		var fichier_image = FileAccess.file_exists("user://illustration_" + str(n) + ".jpg")
		var fichier_audio = FileAccess.file_exists("user://audio_" + str(n) + ".mp3")
		var fichier_titre = FileAccess.file_exists("user://titre_" + str(n) + ".txt")
		if fichier_image == true :
			DirAccess.remove_absolute("user://illustration_"+str(n)+".jpg")
		if fichier_audio == true :
			DirAccess.remove_absolute("user://audio_"+str(n)+".mp3")
		if fichier_titre == true :
			DirAccess.remove_absolute("user://titre_"+str(n)+".txt")
	index = 1
	_chargement_donnees()




#-------------------------------
#Exportation des données
#_______________________________

func _on_button_export_images_pressed():
	$FileDialog_export.show()



func _on_file_dialog_export_dir_selected(dir):
	var adresse_destination = str(dir)
	#var adresse_donnees = DirAccess.open("user://")
	for i in range (1,256):
		var err = FileAccess.file_exists("user://illustration_" + str(i) + ".jpg")
		if err == true :
			DirAccess.copy_absolute("user://illustration_" + str(i) + ".jpg", adresse_destination + "/illustration_" + str(i) + ".jpg")
		var err2 = FileAccess.file_exists("user://audio_" + str(i) + ".mp3")
		if err2 == true :
			DirAccess.copy_absolute("user://audio_" + str(i) + ".mp3", adresse_destination + "/audio_" + str(i) + ".mp3")
		var err3 = FileAccess.file_exists("user://titre_" + str(i) + ".txt")
		if err3 == true :
			DirAccess.copy_absolute("user://titre_" + str(i) + ".txt", adresse_destination + "/titre_" + str(i) + ".txt")
	_chargement_donnees()




#-------------------------------
#Importation des données
#_______________________________

func _on_button_import_donnees_pressed():
	$FileDialog_import.show()




func _on_file_dialog_import_dir_selected(dir):
	var adresse_importation = str(dir)
	print(adresse_importation)
	for i in range (1,256):
		var err = FileAccess.file_exists(adresse_importation +"/illustration_" + str(i) + ".jpg")
		if err == true :
			DirAccess.copy_absolute(adresse_importation + "/illustration_" + str(i) + ".jpg", "user://illustration_" + str(i) + ".jpg")
		var err2 = FileAccess.file_exists(adresse_importation + "/audio_" + str(i) + ".mp3")
		if err2 == true :
			DirAccess.copy_absolute(adresse_importation + "/audio_" + str(i) + ".mp3", "user://audio_" + str(i) + ".mp3")
		var err3 = FileAccess.file_exists(adresse_importation + "/titre_" + str(i) + ".txt")
		if err3 == true :
			DirAccess.copy_absolute(adresse_importation + "/titre_" + str(i) + ".txt", "user://titre_" + str(i) + ".txt")
	index = 1
	_chargement_donnees()



#---------------------- TITRE

func _on_text_edit_focus_exited():
	$TextEdit._sauvegarde_titre(index)



#---------------------- Pictogramme


func _on_button_load_picto_pressed():
	$Panel.show()


func _on_button_annuler_pressed():
	$Panel.hide()


func _on_texture_button_picto_1_pressed():
	_chargement_picto(1)


func _on_texture_button_picto_2_pressed():
	_chargement_picto(2)


func _on_texture_button_picto_3_pressed():
	_chargement_picto(3)


func _on_texture_button_picto_4_pressed():
	_chargement_picto(4)


func _on_texture_button_picto_5_pressed():
	_chargement_picto(5)



func _on_texture_button_picto_6_pressed():
	_chargement_picto(6)


func _on_texture_button_picto_7_pressed():
	_chargement_picto(7)


func _on_texture_button_picto_vide_pressed():
	_picto_vide()


func _chargement_picto(numero):
	$Panel.hide()
	var ma_texture = Texture2D.new()
	ma_texture = load("res://assets/pictos/picto_" + str(numero) + ".png")
	var mon_image = Image.new()
	mon_image = ma_texture.get_image()
	mon_image.save_jpg("user://illustration_" + str(index) + ".jpg")
	_chargement_donnees()


func _picto_vide():
	$Panel.hide()
	var fichier_image = FileAccess.file_exists("user://illustration_" + str(index) + ".jpg")
	if fichier_image == true :
		DirAccess.remove_absolute("user://illustration_" + str(index) + ".jpg")
	_chargement_donnees()






func _changement_mot_de_passe():
	#var code_1 = $TextEdit_code_1.text
	#var code_2 = $TextEdit_code_2.text
	var code_1 = $LineEdit_code_1.text
	var code_2 = $LineEdit_code_2.text
	if code_1 == code_2 && code_1 != "" :
		$Label_code_info2.text = "Le mot de passe a été changé avec succès !"
		$Label_code_info2.set("theme_override_colors/font_color", Color.LIME_GREEN)
		Singleton.Mot_De_Passe = code_1
		Singleton._sauvegarde_mot_de_passe()
	else :
		$Label_code_info2.text = "Il faut 2 saisies identiques pour changer le mot de passe."
		$Label_code_info2.set("theme_override_colors/font_color", Color.RED)


func _on_button_changement_password_pressed():
	_changement_mot_de_passe()






