extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass






func _recuperation_code():
	var code : int
	code = ($gp_boutons/TextureButton_1.valeur * 1000) + ($gp_boutons/TextureButton_2.valeur * 100) + ($gp_boutons/TextureButton_3.valeur * 10) + ($gp_boutons/TextureButton_4.valeur)
	#print(code)
	Singleton.Index_base4 = code
	#var base_10 = Singleton._conversion_base4_vers_base10(code)
	#print("Conversion en base 10 : " + str(base_10) )
	Singleton.Index_base10 = Singleton._conversion_base4_vers_base10(code)
	pass

#func base10_to_base4(nombre):
	#if nombre == 0:
		#return
	#var resultat = ""
	#while nombre > 0:
		#var reste = nombre % 4
		#resultat = str(reste) + resultat
		#nombre = nombre / 4
	#return resultat

func _on_texture_button_validation_pressed():
	_recuperation_code()
	get_tree().change_scene_to_file("res://scenes/lecture.tscn")
	pass # Replace with function body.


func _on_texture_button_retour_pressed():
	get_tree().change_scene_to_file("res://scenes/menu.tscn")

