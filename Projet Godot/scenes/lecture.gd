extends Control

var valeur_base4
var valeur_base10

var nb_1
var nb_2
var nb_3
var nb_4

# Called when the node enters the scene tree for the first time.
func _ready():
	valeur_base10 = Singleton.Index_base10
	$Label_index.text = str(valeur_base10) + " /255"
	valeur_base4 = Singleton._conversion_base10_vers_base4(Singleton.Index_base10)
	_affichage_code_couleur(valeur_base4)
	$ColorRect._chargement_illustration(valeur_base10)
	$Control_audio._chargement_musique(valeur_base10)
	$Control_audio._temps_musique()
	_titre(valeur_base10)
	pass # Replace with function body.

func _titre(nombre):
	var err = FileAccess.file_exists("user://titre_" + str(nombre) + ".txt")
	if err == true :
		var mon_fichier = FileAccess.open("user://titre_" + str(nombre) + ".txt" , FileAccess.READ)
		var mon_contenu = mon_fichier.get_as_text()
		#return mon_contenu
		$Label_titre.text = mon_contenu
	else :
		$Label_titre.text = "  "
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass

#func _affichage_du_code(nombre):
	#var nombre_str = str(nombre)
	#for i in range(nombre_str.length()):
		#
		#pass
	#pass

func _affichage_code_couleur(nombre):
	var str_nombre = str(nombre)
	if str_nombre.length() < 2 : str_nombre = "000" + str_nombre
	if str_nombre.length() < 3 : str_nombre = "00" + str_nombre
	if str_nombre.length() < 4 : str_nombre = "0" + str_nombre
	$gp_boutons/TextureButton_4.valeur = str_nombre[3]
	$gp_boutons/TextureButton_4._changement_couleur()
	$gp_boutons/TextureButton_3.valeur = str_nombre[2]
	$gp_boutons/TextureButton_3._changement_couleur()
	$gp_boutons/TextureButton_2.valeur = str_nombre[1]
	$gp_boutons/TextureButton_2._changement_couleur()
	$gp_boutons/TextureButton_1.valeur = str_nombre[0]
	$gp_boutons/TextureButton_1._changement_couleur()
	pass


func _on_texture_button_retour_pressed():
	get_tree().change_scene_to_file("res://scenes/code.tscn")
	pass # Replace with function body.


